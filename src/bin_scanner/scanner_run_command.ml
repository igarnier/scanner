(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Scanner_logging

type error += RPC_Port_already_in_use of P2p_point.Id.t list

let () =
  register_error_kind
    `Permanent
    ~id:"main.run.port_already_in_use"
    ~title:"Cannot start node: RPC port already in use"
    ~description:"Another tezos node is probably running on the same RPC port."
    ~pp:(fun ppf addrlist ->
      Format.fprintf
        ppf
        "Another tezos node is probably running on one of these addresses \
         (%a). Please choose another RPC port."
        (Format.pp_print_list P2p_point.Id.pp)
        addrlist)
    Data_encoding.(obj1 (req "addrlist" (list P2p_point.Id.encoding)))
    (function RPC_Port_already_in_use addrlist -> Some addrlist | _ -> None)
    (fun addrlist -> RPC_Port_already_in_use addrlist)

let ( // ) = Filename.concat

let init_node ~singleprocess (config : Scanner_config_file.t) =
  (* TODO "WARN" when pow is below our expectation. *)
  ( match config.p2p.discovery_addr with
  | None ->
      lwt_log_notice "No local peer discovery."
      >>= fun () -> return (None, None)
  | Some addr -> (
      Scanner_config_file.resolve_discovery_addrs addr
      >>= function
      | [] ->
          failwith "Cannot resolve P2P discovery address: %S" addr
      | (addr, port) :: _ ->
          return (Some addr, Some port) ) )
  >>=? fun (discovery_addr, discovery_port) ->
  ( match config.p2p.listen_addr with
  | None ->
      failwith "No listen address given"
  | Some addr -> (
      Scanner_config_file.resolve_listening_addrs addr
      >>= function
      | [] ->
          failwith "Cannot resolve P2P listening address: %S" addr
      | (addr, port) :: _ ->
          return (addr, Some port) ) )
  >>=? fun (listening_addr, listening_port) ->
  let listening_addr =
    match listening_addr with
    | addr when Ipaddr.V6.(compare addr unspecified) != 0 ->
        None
    | _ ->
        Some listening_addr
  in
  Scanner_config_file.resolve_bootstrap_addrs config.p2p.bootstrap_peers
  >>= fun trusted_points ->
  Scanner_identity_file.read
    (config.data_dir // Scanner_data_version.default_identity_file_name)
  >>=? fun identity ->
  lwt_log_notice "Peer's global id: %a" P2p_peer.Id.pp identity.peer_id
  >>= fun () ->
  let p2p_config : P2p.config =
    {
      listening_addr;
      listening_port;
      discovery_addr;
      discovery_port;
      trusted_points;
      peers_file =
        config.data_dir // Scanner_data_version.default_peers_file_name;
      private_mode = config.p2p.private_mode;
      greylisting_config = config.p2p.greylisting_config;
      identity;
      proof_of_work_target = Crypto_box.make_target config.p2p.expected_pow;
      disable_mempool = config.p2p.disable_mempool;
      trust_discovered_peers = false;
      disable_testchain = true;
    }
  in
  let p2p_config = (p2p_config, config.p2p.limits) in
  let node_config : Scanner.config = {p2p = p2p_config} in
  Scanner.create ~singleprocess node_config config.shell.peer_validator_limits

let run ?verbosity ~singleprocess (config : Scanner_config_file.t) =
  Scanner_data_version.ensure_data_dir config.data_dir
  >>=? fun () ->
  Lwt_lock_file.create
    ~unlink_on_exit:true
    (Scanner_data_version.lock_file config.data_dir)
  >>=? fun () ->
  (* Main loop *)
  let log_cfg =
    match verbosity with
    | None ->
        config.log
    | Some default_level ->
        {config.log with default_level}
  in
  Internal_event_unix.init
    ~lwt_log_sink:log_cfg
    ~configuration:config.internal_events
    ()
  >>= fun () ->
  lwt_log_notice "Starting the Tezos scanner..."
  >>= fun () ->
  init_node ~singleprocess config
  >>= (function
        | Ok node ->
            return node
        | Error
            (State.Incorrect_history_mode_switch {previous_mode; next_mode}
            :: _) ->
            failwith
              "@[Cannot switch from history mode '%a' to '%a'. Import a \
               context from a corresponding snapshot or re-synchronize a node \
               with an empty tezos node directory.@]"
              History_mode.pp
              previous_mode
              History_mode.pp
              next_mode
        | Error _ as err ->
            Lwt.return err)
  >>=? fun node ->
  (* init_rpc config.rpc node
   * >>=? fun rpc ->
   * lwt_log_notice "The Tezos node is now running!"
   * >>= fun () -> *)
  Lwt_exit.(
    wrap_promise @@ retcode_of_unit_result_lwt @@ Lwt_utils.never_ending ())
  >>= fun retcode ->
  (* Clean-shutdown code *)
  Lwt_exit.termination_thread
  >>= fun x ->
  lwt_log_notice "Shutting down the Tezos node..."
  >>= fun () ->
  Scanner.shutdown node
  >>= fun () ->
  (* lwt_log_notice "Shutting down the RPC server..."
   * >>= fun () ->
   * Lwt_list.iter_p RPC_server.shutdown rpc
   * >>= fun () -> *)
  lwt_log_notice "BYE (%d)" x
  >>= fun () -> Internal_event_unix.close () >>= fun () -> return retcode

let process verbosity singleprocess args =
  let verbosity =
    let open Internal_event in
    match verbosity with [] -> None | [_] -> Some Info | _ -> Some Debug
  in
  let run =
    Scanner_shared_arg.read_and_patch_config_file
      ~ignore_bootstrap_peers:false
      args
    >>=? fun config ->
    Lwt_lock_file.is_locked (Scanner_data_version.lock_file config.data_dir)
    >>=? function
    | false ->
        Lwt.catch
          (fun () -> run ?verbosity ~singleprocess config)
          (function
            | Unix.Unix_error (Unix.EADDRINUSE, "bind", "") ->
                Lwt_list.fold_right_s
                  (fun addr acc ->
                    Scanner_config_file.resolve_rpc_listening_addrs addr
                    >>= fun x -> Lwt.return (x @ acc))
                  config.rpc.listen_addrs
                  []
                >>= fun addrlist -> fail (RPC_Port_already_in_use addrlist)
            | exn ->
                Lwt.return (error_exn exn))
    | true ->
        failwith "Data directory is locked by another process"
  in
  match Lwt_main.run run with
  | Ok (0 | 2) ->
      (* 2 means that we exit by a signal that was handled *)
      `Ok ()
  | Ok _ ->
      `Error (false, "")
  | Error err ->
      `Error (false, Format.asprintf "%a" pp_print_error err)

module Term = struct
  let verbosity =
    let open Cmdliner in
    let doc =
      "Increase log level. Using $(b,-v) is equivalent to using \
       $(b,TEZOS_LOG='* -> info'), and $(b,-vv) is equivalent to using \
       $(b,TEZOS_LOG='* -> debug')."
    in
    Arg.(
      value & flag_all
      & info ~docs:Scanner_shared_arg.Manpage.misc_section ~doc ["v"])

  let singleprocess =
    let open Cmdliner in
    let doc =
      "When enabled, it deactivates block validation using an external \
       process. Thus, the validation procedure is done in the same process as \
       the node and might not be responding when doing extensive I/Os."
    in
    Arg.(
      value & flag
      & info
          ~docs:Scanner_shared_arg.Manpage.misc_section
          ~doc
          ["singleprocess"])

  let term =
    Cmdliner.Term.(
      ret
        ( const process $ verbosity $ singleprocess
        $ Scanner_shared_arg.Term.args ))
end

module Manpage = struct
  let command_description =
    "The $(b,run) command is meant to run the Tezos scanner. Most of its \
     command line arguments corresponds to config file entries, and will have \
     priority over the latter if used."

  let description = [`S "DESCRIPTION"; `P command_description]

  let debug =
    let log_sections =
      String.concat " " (List.rev !Internal_event.Legacy_logging.sections)
    in
    [ `S "DEBUG";
      `P
        ( "The environment variable $(b,TEZOS_LOG) is used to fine-tune what \
           is going to be logged. The syntax is \
           $(b,TEZOS_LOG='<section> -> <level> [ ; ...]') where section is \
           one of $(i," ^ log_sections
        ^ ") and level is one of $(i,fatal), $(i,error), $(i,warn), \
           $(i,notice), $(i,info) or $(i,debug). A $(b,*) can be used as a \
           wildcard in sections, i.e. $(b, client* -> debug). The rules are \
           matched left to right, therefore the leftmost rule is highest \
           priority ." ) ]

  let examples =
    [ `S "EXAMPLE";
      `I ("$(b,Run a node that accepts network connections)", "$(mname) run")
    ]

  let man =
    description @ Scanner_shared_arg.Manpage.args @ debug @ examples
    @ Scanner_shared_arg.Manpage.bugs

  let info = Cmdliner.Term.info ~doc:"Run the Tezos scanner" ~man "run"
end

let cmd = (Term.term, Manpage.info)
