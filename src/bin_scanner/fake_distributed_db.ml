(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2018-2019 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Message = Distributed_db_message

type p2p = (Message.t, Peer_metadata.t, Connection_metadata.t) P2p.net

type connection =
  (Message.t, Peer_metadata.t, Connection_metadata.t) P2p.connection

type 'a request_param = {
  p2p : (Message.t, Peer_metadata.t, Connection_metadata.t) P2p.t;
  data : 'a;
  active : unit -> P2p_peer.Set.t;
  send : P2p_peer.Id.t -> Message.t -> unit;
}

type callback = {
  notify_branch : P2p_peer.Id.t -> Block_locator.t -> unit;
  notify_head : P2p_peer.Id.t -> Block_header.t -> Mempool.t -> unit;
  disconnection : P2p_peer.Id.t -> unit;
}

type db = {
  p2p : p2p;
  p2p_readers : p2p_reader P2p_peer.Table.t;
  active_chains : chain_db Chain_id.Table.t;
      (* disk : State.t;
       * active_chains : chain_db Chain_id.Table.t;
       * protocol_db : Raw_protocol.t;
       * block_input : (Block_hash.t * Block_header.t) Lwt_watcher.input;
       * operation_input : (Operation_hash.t * Operation.t) Lwt_watcher.input; *)
}

and chain_db = {
  (* chain_state : State.Chain.t; *)
  global_db : db;
  (* operation_db : Raw_operation.t; *)
  (* block_header_db : Raw_block_header.t; *)
  (* operation_hashes_db : Raw_operation_hashes.t; *)
  (* operations_db : Raw_operations.t; *)
  mutable callback : callback;
  active_peers : P2p_peer.Set.t ref;
  active_connections : p2p_reader P2p_peer.Table.t;
}

and p2p_reader = {
  gid : P2p_peer.Id.t;
  conn : connection;
  peer_active_chains : chain_db Chain_id.Table.t;
  canceler : Lwt_canceler.t;
  mutable worker : unit Lwt.t;
}

module P2p_reader = struct
  let may_activate global_db state chain_id f =
    match Chain_id.Table.find_opt state.peer_active_chains chain_id with
    | Some chain_db ->
        f chain_db
    | None -> (
      match Chain_id.Table.find_opt global_db.active_chains chain_id with
      | Some chain_db ->
          chain_db.active_peers :=
            P2p_peer.Set.add state.gid !(chain_db.active_peers) ;
          P2p_peer.Table.add chain_db.active_connections state.gid state ;
          Chain_id.Table.add state.peer_active_chains chain_id chain_db ;
          f chain_db
      | None ->
          let meta = P2p.get_peer_metadata global_db.p2p state.gid in
          Peer_metadata.incr meta Unactivated_chain ;
          Lwt.return_unit )

  let deactivate state chain_db =
    chain_db.callback.disconnection state.gid ;
    chain_db.active_peers :=
      P2p_peer.Set.remove state.gid !(chain_db.active_peers) ;
    P2p_peer.Table.remove chain_db.active_connections state.gid

  (* check if the chain advertized by a peer is (still) active *)
  let may_handle global_db state chain_id f =
    match Chain_id.Table.find_opt state.peer_active_chains chain_id with
    | None ->
        let meta = P2p.get_peer_metadata global_db.p2p state.gid in
        Peer_metadata.incr meta Inactive_chain ;
        Lwt.return_unit
    | Some chain_db ->
        f chain_db

  let may_handle_global global_db chain_id f =
    match Chain_id.Table.find_opt global_db.active_chains chain_id with
    | None ->
        Lwt.return_unit
    | Some chain_db ->
        f chain_db

  module Handle_msg_Logging =
  Internal_event.Legacy_logging.Make_semantic (struct
    let name = "scanner.fake_distributed_db.p2p_reader"
  end)

  let soon () =
    let now = Systime_os.now () in
    match Ptime.add_span now (Ptime.Span.of_int_s 15) with
    | Some s ->
        s
    | None ->
        invalid_arg "Fake_distributed_db.handle_msg: end of time"

  let handle_msg global_db state msg =
    let open Message in
    let open Handle_msg_Logging in
    let _meta = P2p.get_peer_metadata global_db.p2p state.gid in
    lwt_debug
      Tag.DSL.(
        fun f ->
          f "Read message from %a: %a"
          -% t event "read_message"
          -% a P2p_peer.Id.Logging.tag state.gid
          -% a Message.Logging.tag msg)
    >>= fun () ->
    match msg with
    | Get_current_branch _chain_id ->
        Lwt.return_unit
        (* Peer_metadata.incr meta @@ Received_request Branch ;
         * may_handle_global global_db chain_id
         * @@ fun chain_db ->
         * if not (Chain_id.Table.mem state.peer_active_chains chain_id) then
         *   Peer_metadata.update_requests meta Branch
         *   @@ P2p.try_send global_db.p2p state.conn
         *   @@ Get_current_branch chain_id ;
         * let seed =
         *   {
         *     Block_locator.receiver_id = state.gid;
         *     sender_id = my_peer_id chain_db;
         *   }
         * in
         * Chain.locator chain_db.chain_state seed
         * >>= fun locator ->
         * Peer_metadata.update_responses meta Branch
         * @@ P2p.try_send global_db.p2p state.conn
         * @@ Current_branch (chain_id, locator) ;
         * Lwt.return_unit *)
    | Current_branch (_chain_id, _locator) ->
        Lwt.return_unit
        (* may_activate global_db state chain_id
         * @@ fun chain_db ->
         * let (head, hist) = (locator :> Block_header.t * Block_hash.t list) in
         * Lwt_list.exists_p
         *   (State.Block.known_invalid chain_db.chain_state)
         *   (Block_header.hash head :: hist)
         * >>= fun known_invalid ->
         * if known_invalid then (
         *   P2p.disconnect global_db.p2p state.conn
         *   >>= fun () ->
         *   P2p.greylist_peer global_db.p2p state.gid ;
         *   Lwt.return_unit )
         * else if Time.System.(soon () < of_protocol_exn head.shell.timestamp)
         * then (
         *   Peer_metadata.incr meta Future_block ;
         *   lwt_log_notice
         *     Tag.DSL.(
         *       fun f ->
         *         f "Received future block %a from peer %a."
         *         -% t event "received_future_block"
         *         -% a Block_hash.Logging.tag (Block_header.hash head)
         *         -% a P2p_peer.Id.Logging.tag state.gid) )
         * else (
         *   chain_db.callback.notify_branch state.gid locator ;
         *   (\* TODO discriminate between received advertisements
         *      and responses? *\)
         *   Peer_metadata.incr meta @@ Received_advertisement Branch ;
         *   Lwt.return_unit ) *)
    | Deactivate _chain_id ->
        Lwt.return_unit
        (* may_handle global_db state chain_id
         * @@ fun chain_db ->
         * deactivate state chain_db ;
         * Chain_id.Table.remove state.peer_active_chains chain_id ;
         * Lwt.return_unit *)
    | Get_current_head _chain_id ->
        Lwt.return_unit
        (* may_handle global_db state chain_id
         * @@ fun chain_db ->
         * Peer_metadata.incr meta @@ Received_request Head ;
         * let {Connection_metadata.disable_mempool; _} =
         *   P2p.connection_remote_metadata chain_db.global_db.p2p state.conn
         * in
         * ( if disable_mempool then
         *   Chain.head chain_db.chain_state
         *   >>= fun head -> Lwt.return (State.Block.header head, Mempool.empty)
         * else State.Current_mempool.get chain_db.chain_state )
         * >>= fun (head, mempool) ->
         * (\* TODO bound the sent mempool size *\)
         * Peer_metadata.update_responses meta Head
         * @@ P2p.try_send global_db.p2p state.conn
         * @@ Current_head (chain_id, head, mempool) ;
         * Lwt.return_unit *)
    | Current_head (_chain_id, _header, _mempool) ->
        Lwt.return_unit
        (* may_handle global_db state chain_id
         * @@ fun chain_db ->
         * let head = Block_header.hash header in
         * State.Block.known_invalid chain_db.chain_state head
         * >>= fun known_invalid ->
         * let {Connection_metadata.disable_mempool; _} =
         *   P2p.connection_local_metadata chain_db.global_db.p2p state.conn
         * in
         * let known_invalid =
         *   known_invalid || (disable_mempool && mempool <> Mempool.empty)
         *   (\* A non-empty mempool was received while mempool is desactivated,
         *        so the message is ignored.
         *        This should probably warrant a reduction of the sender's score. *\)
         * in
         * if known_invalid then (
         *   P2p.disconnect global_db.p2p state.conn
         *   >>= fun () ->
         *   P2p.greylist_peer global_db.p2p state.gid ;
         *   Lwt.return_unit )
         * else if Time.System.(soon () < of_protocol_exn header.shell.timestamp)
         * then (
         *   Peer_metadata.incr meta Future_block ;
         *   lwt_log_notice
         *     Tag.DSL.(
         *       fun f ->
         *         f "Received future block %a from peer %a."
         *         -% t event "received_future_block"
         *         -% a Block_hash.Logging.tag head
         *         -% a P2p_peer.Id.Logging.tag state.gid) )
         * else (
         *   chain_db.callback.notify_head state.gid header mempool ;
         *   (\* TODO discriminate between received advertisements
         *      and responses? *\)
         *   Peer_metadata.incr meta @@ Received_advertisement Head ;
         *   Lwt.return_unit ) *)
    | Get_block_headers _hashes ->
        Lwt.return_unit
        (* Peer_metadata.incr meta @@ Received_request Block_header ;
         * Lwt_list.iter_p
         *   (fun hash ->
         *     read_block_header global_db hash
         *     >>= function
         *     | None ->
         *         Peer_metadata.incr meta @@ Unadvertised Block ;
         *         Lwt.return_unit
         *     | Some (_chain_id, header) ->
         *         Peer_metadata.update_responses meta Block_header
         *         @@ P2p.try_send global_db.p2p state.conn
         *         @@ Block_header header ;
         *         Lwt.return_unit)
         *   hashes *)
    | Block_header _block ->
        Lwt.return_unit
    (* (
     *         let hash = Block_header.hash block in
     *         match find_pending_block_header state hash with
     *         | None ->
     *             Peer_metadata.incr meta Unexpected_response ;
     *             Lwt.return_unit
     *         | Some chain_db ->
     *             Raw_block_header.Table.notify
     *               chain_db.block_header_db.table
     *               state.gid
     *               hash
     *               block
     *             >>= fun () ->
     *             Peer_metadata.incr meta @@ Received_response Block_header ;
     *             Lwt.return_unit ) *)
    | Get_operations _hashes ->
        Lwt.return_unit
        (* Peer_metadata.incr meta @@ Received_request Operations ;
         * Lwt_list.iter_p
         *   (fun hash ->
         *     read_operation global_db hash
         *     >>= function
         *     | None ->
         *         Peer_metadata.incr meta @@ Unadvertised Operations ;
         *         Lwt.return_unit
         *     | Some (_chain_id, op) ->
         *         Peer_metadata.update_responses meta Operations
         *         @@ P2p.try_send global_db.p2p state.conn
         *         @@ Operation op ;
         *         Lwt.return_unit)
         *   hashes *)
    | Operation _operation ->
        Lwt.return_unit
    (* (
     *         let hash = Operation.hash operation in
     *         match find_pending_operation state hash with
     *         | None ->
     *             Peer_metadata.incr meta Unexpected_response ;
     *             Lwt.return_unit
     *         | Some chain_db ->
     *             Raw_operation.Table.notify
     *               chain_db.operation_db.table
     *               state.gid
     *               hash
     *               operation
     *             >>= fun () ->
     *             Peer_metadata.incr meta @@ Received_response Operations ;
     *             Lwt.return_unit ) *)
    | Get_protocols _hashes ->
        Lwt.return_unit
        (* Peer_metadata.incr meta @@ Received_request Protocols ;
         * Lwt_list.iter_p
         *   (fun hash ->
         *     State.Protocol.read_opt global_db.disk hash
         *     >>= function
         *     | None ->
         *         Peer_metadata.incr meta @@ Unadvertised Protocol ;
         *         Lwt.return_unit
         *     | Some p ->
         *         Peer_metadata.update_responses meta Protocols
         *         @@ P2p.try_send global_db.p2p state.conn
         *         @@ Protocol p ;
         *         Lwt.return_unit)
         *   hashes *)
    | Protocol _protocol ->
        Lwt.return_unit
    (* let hash = Protocol.hash protocol in
     *       Raw_protocol.Table.notify
     *         global_db.protocol_db.table
     *         state.gid
     *         hash
     *         protocol
     *       >>= fun () ->
     *       Peer_metadata.incr meta @@ Received_response Protocols ;
     *       Lwt.return_unit *)
    | Get_operation_hashes_for_blocks _blocks ->
        Lwt.return_unit
        (* Peer_metadata.incr meta @@ Received_request Operation_hashes_for_block ;
         * Lwt_list.iter_p
         *   (fun (hash, ofs) ->
         *     State.read_block global_db.disk hash
         *     >>= function
         *     | None ->
         *         Lwt.return_unit
         *     | Some block ->
         *         State.Block.operation_hashes block ofs
         *         >>= fun (hashes, path) ->
         *         Peer_metadata.update_responses meta Operation_hashes_for_block
         *         @@ P2p.try_send global_db.p2p state.conn
         *         @@ Operation_hashes_for_block (hash, ofs, hashes, path) ;
         *         Lwt.return_unit)
         *   blocks *)
    | Operation_hashes_for_block (_block, _ofs, _ops, _path) ->
        Lwt.return_unit
    (*     (
     * match find_pending_operation_hashes state block ofs with
     * | None ->
     *     Peer_metadata.incr meta Unexpected_response ;
     *     Lwt.return_unit
     * | Some chain_db ->
     *     Raw_operation_hashes.Table.notify
     *       chain_db.operation_hashes_db.table
     *       state.gid
     *       (block, ofs)
     *       (ops, path)
     *     >>= fun () ->
     *     Peer_metadata.incr meta
     *     @@ Received_response Operation_hashes_for_block ;
     *     Lwt.return_unit ) *)
    | Get_operations_for_blocks _blocks ->
        Lwt.return_unit
        (* Peer_metadata.incr meta @@ Received_request Operations_for_block ;
         * Lwt_list.iter_p
         *   (fun (hash, ofs) ->
         *     State.read_block global_db.disk hash
         *     >>= function
         *     | None ->
         *         Lwt.return_unit
         *     | Some block ->
         *         State.Block.operations block ofs
         *         >>= fun (ops, path) ->
         *         Peer_metadata.update_responses meta Operations_for_block
         *         @@ P2p.try_send global_db.p2p state.conn
         *         @@ Operations_for_block (hash, ofs, ops, path) ;
         *         Lwt.return_unit)
         *   blocks *)
    | Operations_for_block (_block, _ofs, _ops, _path) ->
        Lwt.return_unit

  (* (
       * match find_pending_operations state block ofs with
       * | None ->
       *     Peer_metadata.incr meta Unexpected_response ;
       *     Lwt.return_unit
       * | Some chain_db ->
       *     Raw_operations.Table.notify
       *       chain_db.operations_db.table
       *       state.gid
       *       (block, ofs)
       *       (ops, path)
       *     >>= fun () ->
       *     Peer_metadata.incr meta @@ Received_response Operations_for_block ;
       *     Lwt.return_unit ) *)

  let rec worker_loop global_db state =
    protect ~canceler:state.canceler (fun () ->
        P2p.recv global_db.p2p state.conn)
    >>= function
    | Ok msg ->
        handle_msg global_db state msg
        >>= fun () -> worker_loop global_db state
    | Error _ ->
        Chain_id.Table.iter
          (fun _ -> deactivate state)
          state.peer_active_chains ;
        P2p_peer.Table.remove global_db.p2p_readers state.gid ;
        Lwt.return_unit

  let run db gid conn =
    let canceler = Lwt_canceler.create () in
    let state =
      {
        conn;
        gid;
        canceler;
        peer_active_chains = Chain_id.Table.create 17;
        worker = Lwt.return_unit;
      }
    in
    (* Chain_id.Table.iter
     *   (fun chain_id _chain_db ->
     *     Lwt.async (fun () ->
     *         let meta = P2p.get_peer_metadata db.p2p gid in
     *         Peer_metadata.incr meta (Sent_request Branch) ;
     *         P2p.send db.p2p conn (Get_current_branch chain_id)))
     *   db.active_chains ; *)
    state.worker <-
      Lwt_utils.worker
        (Format.asprintf "db_network_reader.%a" P2p_peer.Id.pp_short gid)
        ~on_event:Internal_event.Lwt_worker_event.on_event
        ~run:(fun () -> worker_loop db state)
        ~cancel:(fun () -> Lwt_canceler.cancel canceler) ;
    P2p_peer.Table.add db.p2p_readers gid state

  let shutdown s = Lwt_canceler.cancel s.canceler >>= fun () -> s.worker
end

(* let active_peer_ids p2p () =
 *   List.fold_left
 *     (fun acc conn ->
 *       let {P2p_connection.Info.peer_id; _} = P2p.connection_info p2p conn in
 *       P2p_peer.Set.add peer_id acc)
 *     P2p_peer.Set.empty
 *     (P2p.connections p2p) *)

(* let raw_try_send p2p peer_id msg =
 *   match P2p.find_connection p2p peer_id with
 *   | None ->
 *       ()
 *   | Some conn ->
 *       ignore (P2p.try_send p2p conn msg : bool) *)

let create p2p =
  (* let global_request =
   *   {p2p; data = (); active = active_peer_ids p2p; send = raw_try_send p2p}
   * in *)
  let active_chains = Chain_id.Table.create 17 in
  let p2p_readers = P2p_peer.Table.create 17 in
  let db = {p2p; p2p_readers; active_chains} in
  db

let shutdown {p2p_readers; _} =
  P2p_peer.Table.fold
    (fun _peer_id reader acc -> P2p_reader.shutdown reader >>= fun () -> acc)
    p2p_readers
    Lwt.return_unit
